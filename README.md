# Vasil Kotsev

Software Engineer and Consultant with experience in the financial, cross-commodity price reporting, civil engineering, manufacturing and health-management sectors. Worked with clients from all across the globe, while engaging in the whole development lifecycle in order to help them build and maintain their core business systems.